import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from . import mtg

class SearchPopover(Gtk.Popover):

    def __init__(self, my_window, relative_to=None):
        super(SearchPopover, self).__init__(relative_to=relative_to)

        self.build_ui()
        self.my_window = my_window

    def build_ui(self):

        self.model = Gtk.ListStore()
        self.model.set_column_types([str])

        self.completion = Gtk.EntryCompletion()
        self.completion.set_model(self.model)
        self.completion.set_text_column(0)
        self.completion.set_inline_completion(True)
        self.completion.connect("match-selected", self.on_match_selected)

        self.entry = Gtk.SearchEntry()
        self.entry.set_completion(self.completion)
        self.add(self.entry)
        self.entry.connect("activate", self.on_activate)
        self.entry.connect("search-changed", self.on_search_changed)
        self.entry.show()

    def on_activate(self, widget):
        self.finished(self.entry.get_text())

    def finished(self, name):
        self.popdown()

        self.my_window.select_card(name)

    def on_match_selected(self, widget, model, iter):
        self.finished(model.get_value(iter, 0))

    def on_search_changed(self, widget):
        text = self.entry.get_text()

        if len(text) < 3:
            return
        names = mtg.find_by_name(text)
        self.model.clear()
        
        for name in names:
            pos = self.model.append()
            self.model.set_value(pos, 0, name)

class FilterPopover(Gtk.Popover):

    def __init__(self, my_window, relative_to=None):
        super(FilterPopover, self).__init__(relative_to=relative_to)
        
        self.my_window = my_window

        self.build_ui()

    def build_ui(self):
        
        self.entry = Gtk.SearchEntry()
        self.add(self.entry)
        self.entry.connect("activate", self.on_activate)
        self.entry.connect("search-changed", self.on_search_changed)
        self.entry.show()
    
    def on_activate(self, widget):
        self.popdown()

    def on_search_changed(self, widget):
        self.my_window.set_filter(self.entry.get_text())