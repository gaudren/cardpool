import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from . import database, mtg
from .card_row import CardRow
from .popovers import SearchPopover, FilterPopover
from .card_image import CardImage
import pickle
from appdirs import AppDirs
import os.path

class WindowState:

    def __init__(self):
        self.width = 0
        self.height = 0
        self.maximized = False
        self.fullscreen = False

class MyWindow(Gtk.Window):
    
    def __init__(self, db, image_getter, window_state_path, window_state=None):
        super(MyWindow, self).__init__()

        self.db = db

        self.image_getter = image_getter

        self.window_state_path = window_state_path

        self.filter = None

        if window_state is None:
            self.window_state = WindowState()
        else:
            self.window_state = window_state

        self.init_ui()

    def init_ui(self):

        menu = Gtk.Menu()
        menu_item = Gtk.MenuItem(label="About")
        menu.append(menu_item)
        menuBtn = Gtk.MenuButton()
        menuBtn.set_popup(menu)
        menu_item.connect("activate", self.on_menu_activate)

        menu.show_all()

        searchBtn = Gtk.ToolButton()
        searchBtn.set_icon_name("edit-find")
        searchBtn.set_tooltip_text("Search")
        searchBtn.connect("clicked", self.on_search_clicked)   

        self.searchPop = SearchPopover(self, relative_to=searchBtn)

        self.filterBtn = Gtk.ToggleToolButton()
        self.filterBtn.set_icon_name("view-list")
        self.filterBtn.set_tooltip_text("Filter")
        self.filterBtn.connect("clicked", self.on_filter_clicked)

        self.filterPop = FilterPopover(self, relative_to=self.filterBtn)

        headerBar = Gtk.HeaderBar()
        headerBar.pack_start(menuBtn)
        headerBar.pack_start(searchBtn)
        headerBar.pack_end(self.filterBtn)
        headerBar.set_show_close_button(True)
        self.set_titlebar(headerBar)

        self.connect("size-allocate", self.on_size_allocate)
        self.connect("window-state-event", self.on_window_state_event)

        grid = Gtk.Grid()
        self.add(grid)
        
        grid.set_row_spacing(20)
        grid.set_column_spacing(50)
        grid.set_hexpand(True)
        grid.set_halign(Gtk.Align.FILL)
        grid.set_vexpand(True)
        grid.set_valign(Gtk.Align.FILL)

        self.label = Gtk.Label()
        self.label.set_valign(Gtk.Align.END)
        grid.attach(self.label, 0, 0, 1, 1)

        self.previewImage = CardImage()
        self.previewImage.set_size_request(186, 260)
        aspect = Gtk.AspectFrame(ratio=186.0 / 260.0, obey_child=False)
        aspect.add(self.previewImage)
        aspect.set_hexpand(True)
        grid.attach(aspect, 0, 1, 1, 2)

        addBtn = Gtk.Button(label="Add")
        addBtn.set_size_request(200,30)
        addBtn.set_halign(Gtk.Align.CENTER)
        addBtn.set_valign(Gtk.Align.START)
        addBtn.connect("clicked", self.on_button_clicked)
        grid.attach(addBtn, 0, 3, 1, 1)

        self.cardList = Gtk.ListBox()
        self.cardList.set_hexpand(True)
        self.cardList.set_halign(Gtk.Align.FILL)
        self.cardList.set_vexpand(True)
        self.cardList.set_valign(Gtk.Align.FILL)
        self.update_cards(self.db.read_from_db())
        self.cardList.connect("row-selected", self.on_row_selected)

        scrollWindow = Gtk.ScrolledWindow()
        scrollWindow.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scrollWindow.add(self.cardList)
        grid.attach(scrollWindow, 1, 0, 2, 4)

        self.set_border_width(15)
        self.set_title("CARDPOOL")
        self.set_default_size(self.window_state.width, self.window_state.height)

        if self.window_state.maximized:
            self.maximize()

        if self.window_state.fullscreen:
            self.fullscreen()

        self.connect("destroy", self.on_destroy)

    def set_filter(self, filter):
        self.filter = filter
        self.update_cards(self.db.read_from_db(filter=self.filter))

        if self.filter:
            self.filterBtn.set_active(True)
        else:
            self.filterBtn.set_active(False)

    def select_card(self, name):
        self.label.set_text(name)
        image = self.image_getter.get_image(name)
        self.previewImage.load_image(image)

    def on_size_allocate(self, widget, allocation):

        if self.window_state.maximized or self.window_state.fullscreen:
            return

        (width, height) = self.get_size()
        self.window_state.width = width
        self.window_state.height = height

    def on_menu_activate(self, widget):
        dialog = Gtk.AboutDialog()
        dialog.set_title("About CARDPOOL")
        dialog.set_name("CARDPOOL")
        dialog.set_program_name("CARDPOOL")
        dialog.set_version("0.1")
        dialog.set_comments("Tool to organize MTG collection. Card images from Gatherer. Not affiliated with Wizards of the Coast.")
        dialog.set_website("https://gitlab.com/gaudren/cardpool")
        dialog.set_website_label("Repository")
        dialog.set_authors(["Nikki Gaudreau"])
        dialog.set_license_type(Gtk.License.MIT_X11)
        dialog.show_all()

    def on_row_selected(self, widget, row):
        if row is None:
            return

        child = row.get_child()

        if child is not None:
            self.select_card(child.cardName)

    def on_window_state_event(self, widget, event):
        self.window_state.maximized = bool(event.new_window_state & Gdk.WindowState.MAXIMIZED)
        self.window_state.fullscreen = bool(event.new_window_state & Gdk.WindowState.FULLSCREEN)

    def on_button_clicked(self, widget, data=None):
        card = self.label.get_text()

        if len(card) == 0:
            return

        self.db.data_entry(card)
        self.update_cards(self.db.read_from_db())

    def on_search_clicked(self, widget, data=None):
        self.searchPop.popup()

    def on_filter_clicked(self, widget, data=None):
        if self.filterBtn.get_active():
            self.filterPop.popup()
        else:
            self.set_filter(None)

    def on_destroy(self, widget):
        with open(self.window_state_path, "wb") as f:
            pickle.dump(self.window_state, f)
        Gtk.main_quit()

    def update_cards(self, data):
        for child in self.cardList.get_children():
            child.destroy()
        
        for row in data:
            self.cardList.add(CardRow(self, row[1], row[2], row[0]))
        
        self.cardList.show_all()


def main():
    dirs = AppDirs("Cardpool", "hypno.dog")
    os.makedirs(dirs.user_data_dir, exist_ok=True)
    window_state_path = os.path.join(dirs.user_data_dir, "window_state.p")
    database_path = os.path.join(dirs.user_data_dir, "cardpool.db")

    image_cache_path = os.path.join(dirs.user_cache_dir, "images")
    os.makedirs(image_cache_path, exist_ok=True)
    image_getter = mtg.ImageGetter(image_cache_path)

    window_state = None
    try:
        with open(window_state_path, "rb") as f:
            window_state = pickle.load(f)
    except:
        pass

    db = database.Database(database_path)

    db.create_table()
    win = MyWindow(db, image_getter, window_state_path, window_state)
    win.show_all()
    Gtk.main()
    db.close_connection()