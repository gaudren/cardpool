import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class CardRow(Gtk.Box):

    def __init__(self, my_window, name, quantity, pk):
        super(CardRow, self).__init__()

        self.pk = pk
        self.cardName = name
        self.my_window = my_window

        self.label = Gtk.Label(label=name)
        self.pack_start(self.label, True, True, 20)
        self.label.set_xalign(0.0)

        self.quantity = Gtk.SpinButton.new_with_range(0, 999, 1)
        self.quantity.connect("value-changed", self.on_value_changed)
        self.pack_end(self.quantity, False, False, 20)

        self.quantity.set_value(quantity)

    def on_value_changed(self, quantity):
        value = quantity.get_value_as_int()

        if value == 0:
            dialog = Gtk.MessageDialog(parent=self.get_toplevel(), flags=0, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.YES_NO, text="Remove card?")
            dialog.format_secondary_text("Are you sure you want to remove " + self.cardName)
            dialog.connect("response", self.on_dialog_response)
            dialog.run()
            dialog.destroy()
        else:
            self.my_window.db.update_quantity(self.pk, value)

    def on_dialog_response(self, widget, responseId):
        
        if responseId == Gtk.ResponseType.YES:
            self.my_window.db.delete_row(self.pk)
            self.my_window.update_cards(self.my_window.db.read_from_db())
        else:
            self.quantity.set_value(1)