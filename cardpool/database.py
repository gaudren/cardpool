import sqlite3

class Database:

    def __init__(self, path):
        self.connection = sqlite3.connect(path)
        self.cursor = self.connection.cursor()

    def create_table(self):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS cardpool
                            (Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT UNIQUE, Quantity INT);''')

    def data_entry(self, card):
        self.cursor.execute('''INSERT INTO cardpool (Name, Quantity) VALUES(?,?)
                            ON CONFLICT (Name) DO UPDATE SET Quantity = Quantity + ? 
                                WHERE Name = ?''',(card, 1, 1, card))

        self.connection.commit()

    def update_quantity(self, cardId, newQuantity):
        self.cursor.execute(('UPDATE cardpool SET Quantity = ? WHERE Id = ?'), (newQuantity, cardId))

        self.connection.commit()

    def read_from_db(self, filter=None):
        if filter is None or filter == "":
            self.cursor.execute('SELECT * FROM cardpool ORDER BY Name')
        else:
            self.cursor.execute('SELECT * FROM cardpool WHERE UPPER(Name) LIKE "%" || UPPER(?) || "%"', (filter,))

        data = self.cursor.fetchall()
        return data

    def delete_row(self, pk):
        self.cursor.execute('DELETE FROM cardpool WHERE Id = ?',(pk,))

        self.connection.commit()

    def drop_table(self):
        self.cursor.execute('DROP TABLE cardpool')

        self.cursor.close()
        self.connection.close()

    def close_connection(self):
        self.cursor.close()
        self.connection.close()