from mtgsdk import Card
import requests
from cachecontrol import CacheControl
from cachecontrol.caches.file_cache import FileCache

class ImageGetter:

    def __init__(self, path):
        self.session = CacheControl(requests.Session(), cache=FileCache(path))

    def get_image_url(self, name):
        cards = Card.where(name = name).where(page = 1).where(pageSize=10).all()
        found = None

        for card in cards:
            if card.name == name:
                found = card

                break

        return found.image_url

    def get_image(self, name):
        image_url = self.get_image_url(name)
        if image_url is None:
            return None
            
        response = self.session.get(image_url)

        return response.content

def find_by_name(name):
    cards = Card.where(name = name).where(page = 1).all()
    names = set()

    for card in cards:
        names.add(card.name)
    return names