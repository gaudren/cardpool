import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf

class CardImage(Gtk.DrawingArea):

    def __init__(self):
        super(CardImage, self).__init__()
        self.connect("draw", self.on_draw)

        self.back = GdkPixbuf.Pixbuf.new_from_file("back.png")
        self.pixbuf = self.back

    def on_draw(self, widget, context):
        
        width = self.get_allocated_width()
        height = self.get_allocated_height()
        scaled = self.pixbuf.scale_simple(width, height, GdkPixbuf.InterpType.BILINEAR)

        Gdk.cairo_set_source_pixbuf(context, scaled, 0, 0)
        context.paint()

    def load_image(self, buff):

        if buff is None:
            self.pixbuf = self.back
        else:
            loader = GdkPixbuf.PixbufLoader()
            loader.write(buff)
            self.pixbuf = loader.get_pixbuf()
            loader.close()
        self.queue_draw()