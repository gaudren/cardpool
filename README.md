Cardpool
==========

Simple tool to keep track of your MTG collection. Uses Gatherer for card information and images.

Python / GTK

## Screenshot
![Screenshot of UI](./cardpool.png)

## Installation

```
$ git clone https://gitlab.com/gaudren/cardpool
$ cd cardpool
$ python3 -m virtualenv venv
$ . ./venv/bin/activate
$ pip install pygobject mtgsdk pycairo requests cachecontrol[filecache]
$ ./cardpool.py
```